package testsForLambdaDemo;

import lambdaDemo.Gender;
import lambdaDemo.Human;
import lambdaDemo.LambdaRunner;
import lambdaDemo.StreamApiDemo;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class StreamApiDemoTest {
    @Test
    public void testDeleteAllNullObjects(){
        List<Object> list= new ArrayList<>();
        list.add(null);
        list.add(Gender.FEMALE);
        list.add(null);
        list.add(Gender.MALE);

        List<Object> exp = new ArrayList<>();
        Collections.addAll(exp, Gender.FEMALE, Gender.MALE);

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.deleteNullObjects, list));
    }

    @Test
    public void testFindAmountOfPositiveNumbers(){
        Set<Integer> set= new HashSet<>();
        set.add(-1);
        set.add(7);
        set.add(6);
        set.add(3);

        Assert.assertEquals(3, (int)LambdaRunner.run(StreamApiDemo.findAmountOfPositive, set));
    }

    @Test
    public void testFindFirstEven(){
        List<Integer> list1 = new ArrayList<>();
        Collections.addAll(list1, 1, 88, 1, 3, 4, 7, 9);
        Assert.assertEquals(88, (int)LambdaRunner.run(StreamApiDemo.getFirstEven, list1));

        List<Integer> list2 = new ArrayList<>();
        Collections.addAll(list2, 1, 3, 5, 7);
        Assert.assertNull(LambdaRunner.run(StreamApiDemo.getFirstEven, list2));
    }

    @Test
    public void testGetSquaredList(){
        Integer[] array = new Integer[]{
                3, 3, 4, 1, 2, 1
        };
        List<Integer> exp = new ArrayList<>();
        Collections.addAll(exp, 9, 16, 1, 4);

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.getListOfQuadratic, array));
    }

    @Test
    public void testRemoveAllEmptyStringsAndSort(){
        List<String> list = new ArrayList<>();
        Collections.addAll(list, "a4", "", "", "a2", "a3", "a1");

        List<String> exp = new ArrayList<>();
        Collections.addAll(exp, "a1", "a2", "a3", "a4");

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.getSortedList, list));
        for(String s: list){
            System.out.println(s);
        }
    }

    @Test
    public void testSortStringSetReversedOrder(){
        Set<String> set = new HashSet<>();
        Collections.addAll(set, "a3", "a1", "a4", "a2");

        List<String> exp = new ArrayList<>();
        Collections.addAll(exp, "a4", "a3", "a2", "a1");

        Assert.assertEquals(exp, LambdaRunner.run(StreamApiDemo.makeSortedList, set));
    }

    @Test
    public void testGetSumOfSquaredElements(){
        Set<Integer> set = new HashSet<>();
        Collections.addAll(set, 1, 2, 3, 4);
        Assert.assertEquals(30, (int)LambdaRunner.run(StreamApiDemo.sumOfSquaresElements, set));
    }

    @Test
    public void testGetMaxAge(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("", "", "", 33, Gender.FEMALE));
        humans.add(new Human("", "", "", 44, Gender.FEMALE));
        humans.add(new Human("", "", "", 55, Gender.FEMALE));
        humans.add(new Human("", "", "", 22, Gender.FEMALE));
        humans.add(new Human("", "", "", 11, Gender.FEMALE));

        Assert.assertEquals(55, (int)LambdaRunner.run(StreamApiDemo.maxAge, humans));
        Assert.assertNull(LambdaRunner.run(StreamApiDemo.maxAge, new ArrayList<>()));

    }

    @Test
    public void testSortHumans(){
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("", "", "", 33, Gender.FEMALE));
        humans.add(new Human("", "", "", 44, Gender.MALE));
        humans.add(new Human("", "", "", 55, Gender.MALE));
        humans.add(new Human("", "", "", 22, Gender.FEMALE));
        humans.add(new Human("", "", "", 11, Gender.MALE));

        humans = (List<Human>) LambdaRunner.run(StreamApiDemo.sortByAge, humans);
        System.out.println(humans);
    }
}
