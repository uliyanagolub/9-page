package testsForLambdaDemo;

import lambdaDemo.*;
import org.junit.Assert;
import org.junit.Test;

import static lambdaDemo.LambdaRunner.run;
import static org.junit.Assert.assertEquals;


public class LambdaDemoTest{

    public  Human human = new Human("Huston", "Jane", "Third",23, Gender.FEMALE);
    public  Student student = new Student("Harvard", "Biology Faculty",
            "Programmer","Huston", "Jane", "Third", 23, Gender.FEMALE);
    public  Human secondHuman = new Human("Jane", "Jonson", "B", 23, Gender.FEMALE);

    @Test
    public void testGetLength(){
        assertEquals(0, (int) run(LambdaDemo.getLength, ""));
        assertEquals(3, (int) run(LambdaDemo.getLength, "abc"));
        assertEquals(1, (int) run(LambdaDemo.getLength, "a"));
    }

    @Test
    public void testGetFirstSymbol(){
        assertEquals('a', (char) run(LambdaDemo.getFirstSymbol, "abc"));
        assertEquals('.', (char) run(LambdaDemo.getFirstSymbol, ".bc"));
    }

    @Test
    public void testDoesStringConsistSpaces(){
        Assert.assertFalse(run(LambdaDemo.ifBackspaceExist, " "));
        Assert.assertFalse(run(LambdaDemo.ifBackspaceExist, " a b c "));
    }

    @Test
    public void testAmountOfWordsInString(){
        assertEquals(0, (int) run(LambdaDemo.getNumOfWords, ""));
        assertEquals(1, (int) run(LambdaDemo.getNumOfWords, "one"));
        assertEquals(2, (int) run(LambdaDemo.getNumOfWords, "one, two"));
        assertEquals(3, (int) run(LambdaDemo.getNumOfWords, "one, two,eryt"));
    }

    @Test
    public void testGetHumanAge(){
        assertEquals(23, (int) run(LambdaDemo.getHumanAge, student));
    }

    @Test
    public void testIsSecondNamesEqual(){
        Assert.assertTrue(run(LambdaDemo.checkSameSurnames, human, student));
        Assert.assertFalse(run(LambdaDemo.checkSameSurnames, human, secondHuman));
    }

    @Test
    public void testGetFullName(){
        assertEquals("Huston Jane Third", run(LambdaDemo.getFIO, human));
    }

    @Test
    public void testMakeHumanOneYearOlder(){
        Human human = new Human("Jane", "Huston", "Third", 23, Gender.FEMALE);
        Human expected = new Human("Jane", "Huston", "Third", 24, Gender.FEMALE);
        assertEquals(expected, run(LambdaDemo.makeHumanOlder, human));
    }

    @Test
    public void testCheckAllYounger(){
        Human firstHuman = new Human( "Jane", "Huston", "First", 23, Gender.FEMALE);
        Human secondHuman = new Human( "Jane", "Huston", "Second", 23, Gender.FEMALE);
        Human thirdHuman = new Human( "Jane", "Huston", "Third", 23, Gender.FEMALE);
        Assert.assertTrue(LambdaRunner.run(LambdaDemo.humanYoungerMaxEdge, firstHuman, secondHuman, thirdHuman, 34));
    }
}
