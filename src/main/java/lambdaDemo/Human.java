package lambdaDemo;

import java.util.Objects;

public class Human {
// класс Human с полями фамилия, имя, отчество, возраст и пол
    private String surname;
    private String name;
    private String patronymic;
    private int age;
    private Gender gender;

    //конструктор
    public Human(String surname, String name, String patronymic, int age, Gender gender) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.age = age;
        this.gender = gender;
    }
    
    //геттеры, сеттеры
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getAge() == human.getAge() &&
                getSurname().equals(human.getSurname()) &&
                getName().equals(human.getName()) &&
                getPatronymic().equals(human.getPatronymic()) &&
                getGender() == human.getGender();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSurname(), getName(), getPatronymic(), getAge(), getGender());
    }

    @Override
    public String toString() {
        return "Human{" + surname + " " + name + " " + patronymic +
                ", age=" + age + ", " + gender + '}';
    }
}
