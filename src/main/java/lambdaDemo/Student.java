package lambdaDemo;

public class Student extends Human {

    // класс Student с добавленными полями университет, факультет, специальность.
    private String university;
    private String faculty;
    private String speciality;

    // конструктор по параметрам
    public Student(String university, String faculty, String speciality, String surname,
                   String name, String patronymic, int age, Gender gender) {
        super(surname, name, patronymic, age, gender);
        this.faculty = faculty;
        this.university = university;
        this.speciality = speciality;
    }

    // геттеры сеттеры
    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

}
