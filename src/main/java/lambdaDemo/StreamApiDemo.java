package lambdaDemo;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo {

    // в списке объектов удалить все значения null
    public static Function<List<Object>, List<Object>> deleteNullObjects =
            list -> list.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
                    
    // во множестве целых чисел найти количество положительных значений        
    public static Function<Set<Integer>, Integer> findAmountOfPositive =
            set -> Math.toIntExact(set.stream().filter(integer -> integer > 0).count());
            
    // в списке объектов получить последние три элемента        
    public static Function<List<Objects>, List<Objects>> getThreeLastObj =
            list -> list.size() < 3 ? list : list.stream().skip(list.size()-3).collect(Collectors.toList());
            
    /* в списке целых чисел получить первое четное число или значение null, 
    если в списке нет четных чисел*/
    
    public static Function<List<Integer>, Integer> getFirstEven =
            list -> list.stream()
                    .filter(integer -> integer % 2 == 0)
                    .findFirst()
                    .orElse(null);
                    
    //по массиву целых чисел построить список квадратов элементов массива без повторений                
    public static Function<Integer [], List<Integer>> getListOfQuadratic =
    integers -> Arrays.stream(integers)
            .map(integer -> integer*integer)
            .distinct()
            .collect(Collectors.toList());
            
    /*по списку строк построить новый список, содержащий все непустые строки исходного
    списка, упорядоченные по возрастанию*/
    public static Function<List<String>, List<String>> getSortedList =
            list -> list.stream()
                    .filter(string -> !"".equals(string))
                    .sorted()
                    .collect(Collectors.toList());
                    
    // множество строк превратить в список, упорядоченный по убыванию                
    public static Function<Set<String>, List<String>> makeSortedList =
            set -> set.stream()
                    .sorted(Comparator.reverseOrder())
                    .collect(Collectors.toList());
    // для множества целых чисел вычислить сумму квадратов его элементов                
    public static Function<Set<Integer>, Integer> sumOfSquaresElements =
            set -> set.stream()
                    .map(integer -> integer*integer)
                    .reduce(Integer::sum)
                    .orElse(0);
    
    // в коллекции людей вычислите максимальный возраст человека
    public static Function<Collection<Human>, Integer> maxAge =
         col -> col.isEmpty() ? null : col.stream()
                    .max(Comparator.comparing(Human::getAge))
                    .get()
                    .getAge();
                    
    // отсортируйте коллекцию людей сперва по полу, затем — по возрасту                
    public static Function<Collection<Human>, Collection<Human>> sortByGender =
            col -> col.stream().sorted(Comparator.comparing(Human::getGender)).collect(Collectors.toList());
    public static Function<Collection<Human>, Collection<Human>> sortByAge =
            col -> col.stream()
                    .sorted(Comparator.comparing(Human::getGender).thenComparing(Human::getAge))
                    .collect(Collectors.toList());
}
