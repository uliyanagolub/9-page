package lambdaDemo;

import java.util.function.BiFunction;
import java.util.function.Function;

/* класс LambdaRunner со статическими методами, которые получают на вход
лямбда-выражение и подходящий набор параметров, применяют это выражение к
заданным параметрам и возвращают результат. */

public class LambdaRunner {

    public static <T, R> R run (Function<T, R> lambda, T argument){
        return lambda.apply(argument);
    }

    public static <T, U, R> R run(BiFunction<T, U, R> lambda, T arg1, U arg2){
        return lambda.apply(arg1, arg2);
    }

    public static Boolean run(HardFunction<Boolean> lambda, Human human1, Human human2, Human human3, int maxAge){
        return lambda.calculate(human1,human2,human3,maxAge);
    }

}
