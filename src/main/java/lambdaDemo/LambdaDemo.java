package lambdaDemo;

import java.util.function.BiFunction;
import java.util.function.Function;

/* класс LambdaDemo с набором открытых статических неизменяемых полей,
которым в качестве значений присвоены следующие лямбда-выражения */

public class LambdaDemo {
    //для строки символов получить ее длину    
    public static final Function<String, Integer> getLength = String::length;
    
    //для строки символов получить ее первый символ, если он существует, или null иначе
    public static final Function<String, Character> getFirstSymbol =
            string -> ("".equals(string) ? null : string.charAt(0));
            
    // для строки проверить, что она не содержит пробелов    
    public static final Function<String, Boolean> ifBackspaceExist =
            string -> (string == null || !string.contains(" "));
            
    //слова в строке разделены запятыми, по строке получить количество слов в ней     
    public static final Function<String, Integer> getNumOfWords =
            string -> ("".equals(string) ? 0 : string.split(",").length);
            
    //по человеку получить его возраст     
    public static final Function<Human, Integer> getHumanAge =
            human -> (human  == null ? null: human.getAge());
            
    //по двум людям проверить, что у них одинаковая фамилия        
    public static final BiFunction<Human, Human, Boolean> checkSameSurnames =
            (human, human2) -> (human.getSurname().equals(human2.getSurname()));
            
    //получить фамилию, имя и отчество человека в виде одной строки (разделитель — пробел)        
    public static final Function<Human, String> getFIO =
            human -> (human.getSurname()+" "+human.getName()+" "+human.getPatronymic());
            
    //сделать человека старше на один год (по объекту Human создать новый объект)        
    public static final Function<Human,Human> makeHumanOlder =
            human -> (new Human(human.getSurname(), human.getName(), human.getPatronymic(),
                    human.getAge()+1, human.getGender()));
                    
    //по трем людям и заданному возрасту maxAge проверить, что все три человека моложе maxAge                
    public static final HardFunction<Boolean> humanYoungerMaxEdge = (human1, human2, human3, maxAge) ->
            human1.getAge()<maxAge && human2.getAge()<maxAge && human3.getAge()<maxAge;
}
