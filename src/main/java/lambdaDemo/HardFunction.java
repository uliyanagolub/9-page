package lambdaDemo;

public interface HardFunction<T> {
    T calculate(Human human1, Human human2, Human human3, int maxAge);
}
